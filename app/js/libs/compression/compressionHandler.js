import { appendChild, createIterateBtn, createDescriptionParagraph, getCharFrequency, getCharFrequencyWithSmallerOrd, hideElemsWithId } from "../helpers.js";
import { vizualizeEncodingIteration } from "./vizualizeEncodingIteration.js";
import { drawTableTaublkaCetnosti } from "../drawTabulkaCetnosti.js";
import { dataSet } from "../dataset.js";
import { clearPage } from "../clearPage.js";
import { getSVGElemAndAppendToElemWithId } from "../vizuals/drawingFunctions.js";
import { VizualConstants } from "../vizuals/vizualConstants.js";

export function compressionHandler(e) {
    e.preventDefault();

    clearPage();
    hideElemsWithId(["drawing-encoding", "drawing-decoding"]);

    dataSet.originalMessage = document.forms['vstupni-data']['vstupni-retezec'].value;
    let messageToEncode = dataSet.originalMessage;
    if (messageToEncode.length <= 8) {
        dataSet.charFrequency = getCharFrequency(messageToEncode);
        dataSet.charFrequencyWithSmallerOrd = getCharFrequencyWithSmallerOrd(messageToEncode);

        let containerId = 'frequency-table-encoding';
        let description = 'Ze vstupního řetězce se vygeneruje tabulka četnosti. ' +
            'Počet menších znaků představuje počet znaků ze zadaného řetězce, ' +
            'které mají menší ordinální hodnotu v <a href="https://znakynaklavesnici.cz/ascii-tabulka/">ASCII tabulce.</a>';

        let p = createDescriptionParagraph(description);
        appendChild(p, containerId);
        drawTableTaublkaCetnosti(dataSet, containerId, 'tabulka-cetnosti-1');

        description = 'V dalším kroku dojde k aplikaci aritmetického kódování. ' + 
            'Cílem algoritmu je uložit zadaný řetězec na velmi malé čislo, ' + 
            'které leží na polouzavřeném intervalu <0, 1>. Algoritmus dělí podle ' + 
            'zadaného vzorce interval na podintervaly, na základě znaku, ' + 
            'který v dané iteraci spracovává. V každé iteraci algoritmu vznikne nový podinterval, ' +
            'jenž se v následující iteraci použije jako počáteční interval, ' + 
            'který bude opětovně dělen na podintervaly.';

        p = createDescriptionParagraph(description);
        appendChild(p, containerId);

        description = 'Algoritmus takto pokračuje dokud nenarazí na poslední znak. ' +
            'To se zjistí podle délky vstupního řetězce a počtu provedených iterací ' +
            '(Pokud je počet znaků v řetězci roven pořadí současné iterace, jedná se o poslední znak). ' +
            'Na konci vznikne velmi malý interval, z nějž se vybere číslo, které následně reprezentuje ' +
            'celkový vstupní řetězec.';

        p = createDescriptionParagraph(description);
        appendChild(p, containerId);

        description = 'Vzorec dle kterého se interval půlí je následovný: <br>'; 

        p = createDescriptionParagraph(description);
        appendChild(p, containerId);

        // vrat dataSet do vychoziho stavu
        dataSet.lowerBoundary = 0;
        dataSet.upperBoundary = 1;
        dataSet.iteration = 0;
        dataSet.decodedMessage = "";

        const draw = getSVGElemAndAppendToElemWithId('#drawing-encoding', VizualConstants.MAIN_DRAWING_WIDTH, VizualConstants.HEIGHT);
        const btn = createIterateBtn('iterate-encode-btn');
        btn.addEventListener("click", function call_vizualizeEncodingIteration() { vizualizeEncodingIteration(draw, dataSet) } );
        btn.addEventListener("click", function() { window.scrollTo(0, document.body.scrollHeight) });
        appendChild(btn, 'button-container');
    } else {
        alert('Vstupní řetězec musí být dlouhý maximálně 8 znaků.')
    }

}
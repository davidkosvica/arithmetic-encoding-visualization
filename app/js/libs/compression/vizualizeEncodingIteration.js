'use strict';

import { decompressionHandler } from '../decompression/decompressionHandler.js';
import {
    showBlockNode,
    hideBlockNode,
    getNewLowerBoundary,
    getNewUpperBoundary,
    formatNumberToGivenPrecicion,
    createDecodeBtn,
    getNumberBetweenBoundaries,
    createDescriptionParagraph,
    insertAfterNode,
    createHeadingTextLvl4,
    createHtmlContainerContainingEncodedMsg,
    createEncodedMsgWithFinalNum,
    resetBoundaries
} from '../helpers.js';
import {
    drawMainLine,
    drawUpperAndLowerLimitText,
    drawCalculationText,
    drawSubinterval,
    drawNameOfSubinterval,
    drawArrowsToNewIteration,
    increaseHeightOfMainDrawing,
    drawFinalNumberCreation
} from '../vizuals/drawingFunctions.js';


/**
 * 
 * @param {Object} draw 
 * @param {import("../dataset.js").compressionDataSet} dataSet 
 */
export function vizualizeEncodingIteration(draw, dataSet) {
    showBlockNode('drawing-encoding');
    let messageToEncode = dataSet.originalMessage;
    let charIndex = dataSet.iteration;
    let char = messageToEncode[charIndex];
    let charFrequency = dataSet.charFrequency;
    let charFrequencyWithSmallerOrd = dataSet.charFrequencyWithSmallerOrd;
    let prevCharLowerBoundary = dataSet.lowerBoundary;
    let prevCharUpperBoundary = dataSet.upperBoundary;
    let prevCharIntervalLength = prevCharUpperBoundary - prevCharLowerBoundary;
    let iteration = dataSet.iteration;
    increaseHeightOfMainDrawing(draw);
    
    const currentCharLowerBoundary = getNewLowerBoundary(prevCharLowerBoundary, char, charFrequencyWithSmallerOrd, prevCharIntervalLength);
    const currentCharUpperBoundary = getNewUpperBoundary(currentCharLowerBoundary, char, charFrequency, prevCharIntervalLength);
    const currentCharIntervalLength = currentCharUpperBoundary - currentCharLowerBoundary;

    /**
     * @type {import("../typedefs.js").CalculationParameters}
     */
    let calculationsParams = {
        iteration: iteration,
        currentCharLowerBoundary: currentCharLowerBoundary,
        currentCharUpperBoundary: currentCharUpperBoundary,
        currentCharIntervalLength: currentCharIntervalLength,
        prevCharLowerBoundary: prevCharLowerBoundary,
        prevCharUpperBoundary: prevCharUpperBoundary,
        prevCharIntervalLength: prevCharIntervalLength,
        charFrequency: charFrequency,
        charFrequencyWithSmallerOrd: charFrequencyWithSmallerOrd,
        stringLength: messageToEncode.length,
        char: char
    };

    if ((dataSet.iteration) == dataSet.originalMessage.length) {
        let finalNumber = getNumberBetweenBoundaries(prevCharLowerBoundary, prevCharUpperBoundary);
        drawFinalNumberCreation(draw, finalNumber, calculationsParams);

        let text = '`' + formatNumberToGivenPrecicion(prevCharLowerBoundary) + ' lt ' + formatNumberToGivenPrecicion(parseFloat(finalNumber))
            + ' ^^ ' + formatNumberToGivenPrecicion(parseFloat(finalNumber)) + ' lt ' + formatNumberToGivenPrecicion(prevCharUpperBoundary)
            + ' =>` Číslo leží na polouzavřeném intervalu <' + formatNumberToGivenPrecicion(prevCharLowerBoundary)
            + '; ' + formatNumberToGivenPrecicion(currentCharUpperBoundary) + ').';

        const whereNumberLiesDescription = createDescriptionParagraph(text);
        document.getElementById('drawing-encoding').appendChild(whereNumberLiesDescription);
        
        text = 'Číslo <span class="text-danger">' + finalNumber + '</span>' + ' je výsledkem do nějž je zpráva zakódována.';
        const whatFinalNumberRepresents = createDescriptionParagraph(text);
        insertAfterNode(whatFinalNumberRepresents, whereNumberLiesDescription);

        text = 'Na výstupu jsou data z tabulky četnosti (<i>z<sub>1</sub> - n<sub>z<sub>1</sub></sub>, '
            + 'z<sub>2</sub> - n<sub>z<sub>2</sub></sub> ... z<sub>k</sub> - n<sub>z<sub>K</sub></sub></i>, kde <i>z</i> je jednotlivý znak, '
            + '<i>n</i> jeho četnost ve zprávě, <i>k</i> - počet unikátních znaků) a výsledné číslo.';
        const howToInterpretFinalEncodedString = createDescriptionParagraph(text);
        insertAfterNode(howToInterpretFinalEncodedString, whatFinalNumberRepresents);

        const resultInfoHeader = createHeadingTextLvl4('Výstup komprese:');
        insertAfterNode(resultInfoHeader, howToInterpretFinalEncodedString);

        const finalEncodedMsg = createEncodedMsgWithFinalNum(finalNumber, charFrequency);
        const htmlContainerWithEncodedMsg = createHtmlContainerContainingEncodedMsg(finalEncodedMsg);
        insertAfterNode(htmlContainerWithEncodedMsg, resultInfoHeader);

        const decodeBtn = createDecodeBtn('decode-btn');
        dataSet.encodedMessage = finalEncodedMsg;
        dataSet.encodedNumber = parseFloat(finalNumber);
        dataSet = resetBoundaries(dataSet);
        decodeBtn.addEventListener("click", function() { decompressionHandler(dataSet) } );
        insertAfterNode(decodeBtn, htmlContainerWithEncodedMsg);

        hideBlockNode('iterate-encode-btn');

    } else if ((dataSet.iteration) < dataSet.originalMessage.length) {
        drawMainLine(draw, calculationsParams);
        drawUpperAndLowerLimitText(draw, calculationsParams);
        drawCalculationText(draw, calculationsParams);
        drawSubinterval(draw, calculationsParams);
        drawNameOfSubinterval(draw, calculationsParams);
        drawArrowsToNewIteration(draw, calculationsParams);
        
        dataSet.lowerBoundary = currentCharLowerBoundary;
        dataSet.upperBoundary = currentCharUpperBoundary;
        calculationsParams['iteration']++; 
        dataSet.iteration++;
        drawMainLine(draw, calculationsParams);
    } else {
        return;
    }
    
    // @ts-ignore
    MathJax.typeset();
}
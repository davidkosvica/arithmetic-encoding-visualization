import { vizualizeDecodingIteration } from './vizualizeDecodingIteration.js';
import { drawTableTaublkaCetnosti } from '../drawTabulkaCetnosti.js';
import { createIterateBtn, appendChild, hideElemsWithId } from '../helpers.js';

/**
 * Starts iteration of decompression operation
 * @param {Object} dataSet
 */
export function decompressionHandler(dataSet) {
    event.preventDefault();

    drawTableTaublkaCetnosti(dataSet, 'frequency-table-decoding', 'tabulka-cetnosti2');
    dataSet.iteration = 1;

    const btn = createIterateBtn('iterate-decode-btn');
    btn.addEventListener("click", function call_vizualizeDecodingIteration() { vizualizeDecodingIteration(dataSet) } );
    btn.addEventListener("click", function() { window.scrollTo(0, document.body.scrollHeight) });
    appendChild(btn, 'button-container');
    hideElemsWithId(["decode-btn"]);
}
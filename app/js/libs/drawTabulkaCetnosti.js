'use strict';

export function drawTableTaublkaCetnosti(dataObj, idContainerEl, idOfTbl) {
    let charFrequency = dataObj.charFrequency;
    let charFrequencyWithSmallerOrd = dataObj.charFrequencyWithSmallerOrd;

    let tableToRemove = document.getElementById(idOfTbl);
    if (tableToRemove) {
        tableToRemove.remove();
    }

    let table = document.createElement("table");
    
    table.id = idOfTbl;
    table.classList.add('table', 'table-bordered', 'table-sm');

    let container = document.getElementById(idContainerEl)
    container.appendChild(table);

    let tableHeaderRow = document.createElement("tr");
    let tableHeaderCell = document.createElement("th");

    tableHeaderCell.innerHTML = "Znak";
    tableHeaderRow.appendChild(tableHeaderCell);
    tableHeaderCell = document.createElement("th");

    tableHeaderCell.innerHTML = "Počet znaků (n<sub>znak</sub>)";
    tableHeaderRow.appendChild(tableHeaderCell);
    tableHeaderCell = document.createElement("th");

    tableHeaderCell.innerHTML = "Počet menších znaků (m<sub>znak</sub>)";
    tableHeaderRow.appendChild(tableHeaderCell);
    table.appendChild(tableHeaderRow);

    for (let char in charFrequency) {
        if (char === 'stringLength') {
            continue;
        }

        let tableRow = document.createElement("tr");

        let tableCell = document.createElement("td");
        tableCell.innerHTML = char;
        tableRow.appendChild(tableCell);

        tableCell = document.createElement("td");
        tableCell.innerHTML = charFrequency[char];
        tableRow.appendChild(tableCell);

        tableCell = document.createElement("td");
        tableCell.innerHTML = charFrequencyWithSmallerOrd[char];
        tableRow.appendChild(tableCell);

        table.appendChild(tableRow);
    }
 
    let tableRow = document.createElement("tr");
    let tableCell = document.createElement("td");
    tableCell.innerHTML = "Celkem";
    tableRow.appendChild(tableCell);
    tableCell = document.createElement("td");
    tableCell.innerHTML = charFrequency['stringLength'];
    tableRow.appendChild(tableCell);
    tableRow.classList.add("table-info");
    table.appendChild(tableRow);
}

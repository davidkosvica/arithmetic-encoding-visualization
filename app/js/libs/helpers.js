'use strict';

export function updateCounter() {
    const string = document.forms['vstupni-data']['vstupni-retezec'].value;
    document.getElementById('charCounter').innerText = string.length;
    if (string.length > 8) {
        document.getElementById('charCounter').classList.add('text-danger');
    } else {
        document.getElementById('charCounter').classList.add('text-info');
    }
}

/**
 * @param {number} encodedNumber
 * @param {IntervalBoundaries} intervalBoundaries 
 */
export function isEncodedNumberInInterval(encodedNumber, intervalBoundaries) {
    if ((encodedNumber >= intervalBoundaries.lowerBoundary) && (encodedNumber < intervalBoundaries.upperBoundary)) {
        return true;
    } else {
        return false;
    }
}

/**
 * returns HTML element with specified attributes
 * @param {string} type - name of the element (div, span, ul, li ...) 
 * @param {string[]} [classes] - optional array of classes that will be in the class="<class-1>, <class-2> ..." of the element
 */
export function createHtmlElement(type, classes) {
    // osetrim nepovinny parametr, v pripade ze obsahuje null
    // prirad prazdne pole
    classes = (typeof classes === 'undefined') ? [] : classes;
    const el = document.createElement(type);
    classes.forEach(function (item) {
        el.classList.add(item);
    })

    return el;
}

/**
 * sets "display: none" to elements with specified ids
 * @param {string[]} ids - array of ids that belong to elements that will be hidden
 */
export function hideElemsWithId(ids) {
    for (let i = 0; i < ids.length; i++) {
        let element = document.getElementById(ids[i]);
        element.style.display = "none";
    }
}

/**
 * Creates description for the final step in decoding
 * @param {string} decodedMessage - contains decoded message
 */
export function createFinalDecodeDescription(decodedMessage) {
    const containerDiv = createHtmlElement("div", ["metatext", "col-sm-12"]);
    const headerP = createParagraphElement("Dekódovaná zpráva je:", ["h4"]);
    const alertDiv = createHtmlElement("div", ['alert', 'alert-success']);
    const resultInfoP = createHtmlElement('p', ['lead']);
    
    containerDiv.appendChild(headerP);
    alertDiv.innerHTML = decodedMessage;
    resultInfoP.appendChild(alertDiv);
    containerDiv.appendChild(resultInfoP);
    appendChild(containerDiv, 'drawing-decoding');
}

/**
 * 
 * @param {string} text - text that the paragraph will contain
 * @param {string[]} [classes] - optional array of classes that will be in the class="<class-1>, <class-2> ..." of the element
 */
export function createParagraphElement(text, classes) {
    // osetrim nepovinny parametr, v pripade ze obsahuje null
    // prirad prazdne pole
    classes = (typeof classes === 'undefined') ? [] : classes;

    const p = createHtmlElement("p", classes);
    p.innerHTML = text;

    return p;
}

/**
* Inserts node before node with given id.
* @param {HTMLElement} newNode
* @param {string} referenceNodeId
*/
export function insertBefore(newNode, referenceNodeId) {
    const referenceNode = document.getElementById(referenceNodeId);
    referenceNode.parentNode.insertBefore(newNode, referenceNode);
}

/**
* Inserts node after node with given id.
* @param {HTMLElement} newNode
* @param {string} referenceNodeId
*/
export function insertAfter(newNode, referenceNodeId) {
    const referenceNode = document.getElementById(referenceNodeId);
    return referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}


/**
* Inserts node after given node.
* @param {HTMLElement} newNode
* @param {HTMLElement} referenceNode
*/
export function insertAfterNode(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

/**
* Inserts node before node with given id.
* @param {HTMLElement} childNode
* @param {string} parentId
*/
export function appendChild(childNode, parentId) {
    const parent = document.getElementById(parentId);
    parent.appendChild(childNode);
}

/**
* Returns button, which executes iteration of arithmetic encoding.
* @param {string} btnId
* @returns {HTMLElement}
*/
export function createIterateBtn(btnId) {
    const iterateButton = document.createElement('button');
    iterateButton.id = btnId;
    iterateButton.classList.add('btn', 'btn-secondary');
    iterateButton.innerHTML = 'Další krok';

    return iterateButton
}

/**
* Returns button, which executes iteration of arithmetic encoding.
* @param {string} btnId
* @returns {HTMLElement}
*/
export function createDecodeBtn(btnId) {
    const decodeButton = document.createElement('button');
    decodeButton.id = btnId; //'decode-btn';
    decodeButton.classList.add("btn", "btn-success");
    decodeButton.innerHTML = "Dekomprimuj";

    return decodeButton;
}

/**
* Returns element that contains description for each step.
* @param {string} text
* @returns {HTMLElement}
*/
export function createDescriptionParagraph(text) {
    const p = document.createElement('p');

    p.classList.add('metatext');
    p.innerHTML = text;

    return p;
}

/**
* Returns lvl 4 header element that contains given string.
* @param {string} text
* @returns {HTMLElement}
*/
export function createHeadingTextLvl4(text) {
    const resultInfoHeader = document.createElement("p");
    resultInfoHeader.classList.add("h4");
    resultInfoHeader.innerHTML = text;

    return resultInfoHeader;
}

/**
* Returns element for the final encoded message
* @returns {HTMLElement}
*/
export function createEncodedMsgContainer() {
    const resultInfoParagraph = document.createElement("p");
    resultInfoParagraph.classList.add("lead");
    resultInfoParagraph.id = 'encoded-msg';

    return resultInfoParagraph;
}

/**
* Returns alert element with given text
* @returns {HTMLElement}
*/
export function createAlertElement(text) {
    const alert = document.createElement("div");
    alert.classList.add('alert', 'alert-primary');
    alert.innerHTML = text;

    return alert;
}

/**
 * returns <div class="flex-container"></div>
 * @return {HTMLElement}
 */
export function createFlexContainer() {
    const containerDiv = document.createElement('div');
    containerDiv.classList.add("flex-container");

    return containerDiv;
}



/**
* Returns final encoded message in the format: 
* (letter1 - frequency, letter2 - frequency ... letterN - frequency, finalNumber)
* @param {string} finalNumber
* @param {Object} charFrequency
* @returns {String}
*/
export function createEncodedMsgWithFinalNum(finalNumber, charFrequency) {
    let outputString = '';
    for (let char in charFrequency) {
        if (char == 'stringLength') {
            continue;
        }

        if (char == ' ') {
            outputString += '&nbsp-' + charFrequency[char].toString() + ', ';

        } else {
            outputString += char + '-' + charFrequency[char].toString() + ', ';
        }
    }

    // outputString += finalNumber.toString();
    outputString += finalNumber;
    return outputString;
}


/**
* Returns html element that contains description for each step.
* @returns {HTMLElement}
*/
export function createHtmlContainerContainingEncodedMsg(finalMsg) {
    const finalMessageContainer = createEncodedMsgContainer();
    const alertElement = createAlertElement(finalMsg);
    finalMessageContainer.appendChild(alertElement);
    
    return finalMessageContainer;
}

/**
* Makes block element with given id appear.
* @param {string} nodeId
*/
export function showBlockNode(nodeId) {
    let node = document.getElementById(nodeId);
    node.style.display = 'block';
}

/**
* Makes block element with given id disappear.
* @param {string} nodeId
*/
export function hideBlockNode(nodeId) {
    let node = document.getElementById(nodeId);
    node.style.display = 'none';
}

/**
 * 
 * @param {import("./dataset.js").compressionDataSet} dataSet 
 */
export function resetBoundaries(dataSet) {
    dataSet.lowerBoundary = 0;
    dataSet.upperBoundary = 1;

    return dataSet;
}


/**
* Returns number of character from the message thats going to be
* encoded and returns number of character with smaller ASCII value
* @param {string} inputChar
* @param {string} msgToBeEncoded
* @returns {number}
*/
export function getNumOfCharWithSmallerOrd(inputChar, msgToBeEncoded) {
    let num = 0
    for (let char of msgToBeEncoded) {
        if (inputChar.charCodeAt(0) > char.charCodeAt(0)) {
            num++;
        }
    }
    return num;
}

/**
 * Returns number of characters that are the same as character in inputChar param
 * from the given string in the msgToBeEncoded param
 * @param {string} inputChar 
 * @param {string} msgToBeEncoded 
 * @returns {number}
 */
export function getNumberOfEqualCharsFromMsg(inputChar, msgToBeEncoded) {
    let num = 0
    for (let char of msgToBeEncoded) {
        if (inputChar.charCodeAt(0) == char.charCodeAt(0)) {
            num++;
        }
    }
    return num;
}

/**
 * Returns string that contains given number in num param
 * formated to specified precision
 * @param {number} num 
 */
export function formatNumberToGivenPrecicion(num) {
    const precision = 9;
    if (num.toString().length > precision) {
        return Number.parseFloat(num.toString()).toPrecision(precision);
    } else {
        return num.toString();
    }
}

/**
 * Creates and returns html element with given content and with assigned classes
 * (<h4 class="iteration-header mt-4">iteration + '. iterace'<h4>)
 * @param {string} iteration - Ordinal number representing nth iteration 
 * @returns {HTMLElement}
 */
export function createHTMLElemIterationHeader(iteration) {
    const iterHdr = document.createElement('h4');
    iterHdr.classList.add('iteration-header', 'mt-4');
    iterHdr.innerHTML = iteration + '. iterace:';

    return iterHdr;
}

/**
 * Calculates lower boundary of an interval used in arithmetic compression
 * @param {number} lowerBoundary - lower boundary of an interval for arithmetic compression
 * @param {string} char - character to be encoded from the original message
 * @param {Object.<string, number>} charFrequencyWithSmallerOrd - frequencies of each character with 
 * smaller ASCII value than a character to be encoded in the original message 
 * @param {number} intervalLength - difference between lower boundary and upper boundary of the interval
 * @return {number}
 */
export function getNewLowerBoundary(lowerBoundary, char, charFrequencyWithSmallerOrd, intervalLength) {
    let boundary = lowerBoundary + ((charFrequencyWithSmallerOrd[char] / charFrequencyWithSmallerOrd['stringLength']) * intervalLength);

    return boundary;
}

/**
 * Calculates upper boundary of an interval used in arithmetic compression
 * @param {number} lowerBoundary - lower boundary of an interval for arithmetic compression
 * @param {string} char - character to be encoded from the original message
 * @param {Object.<string, number>} charFrequency - frequencies of each character in the original message 
 * @param {number} intervalLength - difference between lower boundary and upper boundary of the interval
 * @return {number}
 */
export function getNewUpperBoundary(lowerBoundary, char, charFrequency, intervalLength) {
    let boundary = lowerBoundary + ((charFrequency[char] / charFrequency['stringLength']) * intervalLength);

    return boundary;
}

/**
 * Returns object that contains frequency of each letter in the string
 * @param {string} msgToBeEncoded - string containing msg to be encoded
 * @return {Object.<string, number>}
 */
export function getCharFrequency(msgToBeEncoded) {
    let charFrequency = { 'stringLength': msgToBeEncoded.length };
    for (let char of msgToBeEncoded) {
        let numberOfChar = getNumberOfEqualCharsFromMsg(char, msgToBeEncoded);
        charFrequency[char] = numberOfChar;
    }

    return charFrequency;
}

/**
 * Returns object that contains frequency of each letter in the string
 * that has smaller ASCII value
 * @param {string} msgToBeEncoded - string containing msg to be encoded
 * @return {Object.<string, number>}
 */
export function getCharFrequencyWithSmallerOrd(msgToBeEncoded) {
    let charFrequencyWithSmallerOrd = { 'stringLength': msgToBeEncoded.length };
    for (let char of msgToBeEncoded) {
        let numberOfChar = getNumOfCharWithSmallerOrd(char, msgToBeEncoded);
        charFrequencyWithSmallerOrd[char] = numberOfChar;
    }

    return charFrequencyWithSmallerOrd;
}

/**
 * Returns html encoded representation of a given character
 * @param {string} s
 * @returns {string} 
 */
export function HtmlEncode(s) {
    var el = document.createElement("div");
    el.innerText = el.textContent = s;
    s = el.innerHTML;

    return s;
}

/**
 * Extracts encoded number representing original data
 * from encoded message
 * @param {string} message 
 */
export function extractEncodedNumber(message) {
    let inputElements = message.split(",");
    let finalNumber = null;
    inputElements.forEach(function (item) {
        let charAndFreq = item.trim().split('-');
        if (charAndFreq.length == 1) {
            finalNumber = parseFloat(charAndFreq[0]);
        }
    })

    return finalNumber;

}

/**
* Receives two numbers (boundaries of interval) and returns
* number that consists of the same first digits that the numbers share
* plus one digit from the second number (upperBoundary) and index of that
* digit. That makes sure that the number thats returned lies between
* the two boundary numbers.
*
* @param {number} lowerBoundary
* @param {number} upperBoundary
* @returns {string}
*/
export function getNumberBetweenBoundaries(lowerBoundary, upperBoundary) {
    let finalNumber = '';
    let index;

    for (index = 0; index < upperBoundary.toString().length; index++) {
        finalNumber += upperBoundary.toString()[index];
        if (upperBoundary.toString()[index] != lowerBoundary.toString()[index]) {
            break;
        } 
    }

    return finalNumber;
}

/**
 * @param {import("./compression/vizualizeEncodingIteration").CalculationParameters} calculationParams
 * @returns {string}
 */
export function getLowerBoundaryEquation(calculationParams) {
    const iteration = calculationParams.iteration;
    const stringLength = calculationParams.stringLength;
    const prevCharLowerBoundary = calculationParams.prevCharLowerBoundary;
    const currentCharLowerBoundary = calculationParams.currentCharLowerBoundary;
    const char = calculationParams.char;
    const intervalLength = calculationParams.prevCharIntervalLength;
    const charFrequencyWithSmallerOrd = calculationParams.charFrequencyWithSmallerOrd;

    let lowerBoundaryEquation = '<p>`D_' + iteration.toString() + ' = D_' + (iteration - 1).toString() +
        ' + frac{m_' + HtmlEncode(char) + '}{n} * L_' + (iteration - 1).toString() + 
        ' = ' + formatNumberToGivenPrecicion(prevCharLowerBoundary) + ' + frac{' + charFrequencyWithSmallerOrd[char] + 
        '}{' + formatNumberToGivenPrecicion(stringLength) + '} * ' + formatNumberToGivenPrecicion(intervalLength) + 
        ' = ' + formatNumberToGivenPrecicion(currentCharLowerBoundary) + '`</p>';

    return lowerBoundaryEquation;
}

/**
 * @param {import("./compression/vizualizeEncodingIteration").CalculationParameters} calculationParams
 * @returns {string}
 */
export function getUpperBoundaryEquation(calculationParams) {
    const iteration = calculationParams.iteration;
    const stringLength = calculationParams.stringLength;
    const currentCharUpperBoundary = calculationParams.currentCharUpperBoundary;
    const currentCharLowerBoundary = calculationParams.currentCharLowerBoundary;
    const char = calculationParams.char;
    const charFrequency = calculationParams.charFrequency;
    const intervalLength = calculationParams.prevCharIntervalLength;
    
    const upperBoundaryEquation = '<p>`H_' + iteration.toString() + ' = D_' + iteration.toString() +
        ' + frac{n_' + HtmlEncode(char) + '}{n} * L_' + (iteration - 1).toString() + 
        ' = ' + formatNumberToGivenPrecicion(currentCharLowerBoundary) + ' + frac{' + charFrequency[char] + 
        '}{' + formatNumberToGivenPrecicion(stringLength) + '} * ' + formatNumberToGivenPrecicion(intervalLength) + 
        ' = ' + formatNumberToGivenPrecicion(currentCharUpperBoundary) + '`</p>';

    return upperBoundaryEquation;
}

/**
 * @param {import("./compression/vizualizeEncodingIteration").CalculationParameters} calculationParams
 * @returns {string}
 */
export function getLengthEquation(calculationParams) {
    const iteration = calculationParams.iteration;
    const currentCharUpperBoundary = calculationParams.currentCharUpperBoundary;
    const currentCharLowerBoundary = calculationParams.currentCharLowerBoundary;
    const currentCharIntervalLength = calculationParams.currentCharIntervalLength;
    
    const lengthEquation = '<p>`L_' + iteration.toString() + ' = H_' + iteration.toString() + 
        ' - ' + 'D_' + iteration.toString() + ' = ' + formatNumberToGivenPrecicion(currentCharUpperBoundary) + 
        ' - ' + formatNumberToGivenPrecicion(currentCharLowerBoundary) + ' = ' + 
        formatNumberToGivenPrecicion(currentCharIntervalLength) + '`</p>';

    return lengthEquation;
}
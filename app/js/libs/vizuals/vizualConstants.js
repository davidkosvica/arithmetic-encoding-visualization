/**
 * Enum for graphical positioning values.
 * @readonly
 * @enum {number}
 */

export const VizualConstants = {
    /** width of svg element containing vizualization */
    MAIN_DRAWING_WIDTH: 1800,
    /** height of the svg element containing vizualization */
    MAIN_DRAWING_HEIGHT: 200,
    /** width of interval line */
    WIDTH: 1000,
    /** height of vizual area containing interval line */
    HEIGHT: 100, 
    /** distance between each vizual area containing interval line */
    VERTICAL_DISTANCE: 300, 
    /** vertical shift of every element downwards */
    VERTICAL_SHIFT: 50, 
    /** horizontal shift of every element to the left */
    HORIZONTAL_SHIFT: 40,
} 

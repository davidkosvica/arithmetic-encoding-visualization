/**
* Clears page from last program execution.
*/
export function clearPage() {
    const containerIds = ["frequency-table-encoding", "frequency-table-decoding", "drawing-encoding", "drawing-decoding", "button-container"];

    for (let i = 0; i < containerIds.length; i++) {
        let container = document.getElementById(containerIds[i]);
        while (container.firstChild) {
            container.removeChild(container.firstChild);
        }
    }
}
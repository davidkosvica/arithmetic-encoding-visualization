
/**
* Dataset that contains working data for arithmetic compression
* @type {import("./typedefs.js").CompressionDataSet}
*/
export let dataSet = {
    originalMessage: "",
    encodedMessage: "",
    encodedNumber: -1, // meaningless value to initialize variable
    charFrequency: {},
    charFrequencyWithSmallerOrd: {},
    lowerBoundary: 0,
    upperBoundary: 1,
    decodedMessage: "",
    iteration: 0,
    EPSILON: 0.000000000001
}
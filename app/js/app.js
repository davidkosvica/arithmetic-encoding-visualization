'use strict';

import { updateCounter } from './libs/helpers.js';
import { compressionHandler } from './libs/compression/compressionHandler.js';

document.forms['vstupni-data']['vstupni-retezec'].addEventListener('input', updateCounter);
document.getElementById('input-form-encode').addEventListener('submit', compressionHandler);
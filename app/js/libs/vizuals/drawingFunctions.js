import { VizualConstants } from "./vizualConstants.js";
import {
    formatNumberToGivenPrecicion as formatNumberToGivenPrecision,
    HtmlEncode,
    getNewLowerBoundary,
    getNewUpperBoundary,
    createParagraphElement,
    createFlexContainer,
    getLowerBoundaryEquation,
    getUpperBoundaryEquation,
    getLengthEquation,
    createHtmlElement,
    appendChild
} from "../helpers.js"

/**
 * @param {string}  idOfHTMLContainer
 * @param {number}  width
 * @param {number}  height
 * @return {Object} drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 */
export function getSVGElemAndAppendToElemWithId(idOfHTMLContainer, width, height) {
    // @ts-ignore
    const drawing = SVG().addTo(idOfHTMLContainer).size(width, height);

    return drawing;
}

/**
 * @param {Object}  drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 */
export function increaseHeightOfMainDrawing(drawing) {
    drawing.attr('height', drawing.attr('height') + VizualConstants.VERTICAL_DISTANCE);
}

/**
 * @param {Object}  drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 */
function getArrowMarker(drawing) {
    return drawing.marker(40, 10, function (add) {

        add.line(0, 2, 40, 5).stroke({ color: "grey" })
        add.line(0, 8, 40, 5).stroke({ color: "grey" })

    }).size(40, 10).ref(40, 5);
}

/**
 * @param {Object}  drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 */
function getDotMarker(drawing) {
    return drawing.marker(5, 5, function (add) {

        add.circle(5).fill("red");

    }).size(5, 5).ref(2.5, 2.5);
}

/**
 * @param {Object} drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 * @param {number} x1 - line beginning x coordinate
 * @param {number} y1 - line beginning y coordinate
 * @param {number} x2 - line ending x coordinate
 * @param {number} y2 - line ending y coordinate 
 */
function drawLine(drawing, x1, y1, x2, y2) {
    const attributes = {
        width: 2,
        color: "black"
    }

    drawing.line(x1, y1, x2, y2).stroke(attributes);
}

/**
 * @param {Object} drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 * @param {import("../compression/vizualizeEncodingIteration").CalculationParameters} calculationParams
 */
export function drawMainLine(drawing, calculationParams) {
    const x1 = 0 + VizualConstants.HORIZONTAL_SHIFT;
    const y1 = VizualConstants.VERTICAL_DISTANCE * calculationParams.iteration + VizualConstants.VERTICAL_SHIFT;
    const x2 = VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    const y2 = y1;

    drawLine(drawing, x1, y1, x2, y2);
}

/**
 * @param {Object} drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 */
export function drawMainLineForDecoding(drawing) {
    const x1 = 0 + VizualConstants.HORIZONTAL_SHIFT;
    const y1 = VizualConstants.HEIGHT;
    const x2 = VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    const y2 = y1;

    drawLine(drawing, x1, y1, x2, y2);
}

/**
 * @param {Object}  drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 * @param {import("../compression/vizualizeEncodingIteration").CalculationParameters} calculationParams
 */
export function drawBoundaryTextForDecoding(drawing, calculationParams) {
    let xCoordinate1 = 0 + VizualConstants.HORIZONTAL_SHIFT;
    const yCoordinate1 = VizualConstants.VERTICAL_SHIFT;
    const outputTextLowerBoundary = '<' + formatNumberToGivenPrecision(calculationParams.prevCharLowerBoundary);
    const outputTextUpperBoundary = formatNumberToGivenPrecision(calculationParams.currentCharUpperBoundary) + ')';

    drawing.text(outputTextLowerBoundary).attr({ x: xCoordinate1, y: yCoordinate1 });
    xCoordinate1 = VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    drawing.text(outputTextUpperBoundary).attr({ x: xCoordinate1, y: yCoordinate1 });
}

/**
 * @param {Object}  drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 * @param {import("../compression/vizualizeEncodingIteration").CalculationParameters} calculationParams
 */
export function drawUpperAndLowerLimitText(drawing, calculationParams) {
    let xCoordinate1 = 0 + VizualConstants.HORIZONTAL_SHIFT;
    const yCoordinate1 = VizualConstants.VERTICAL_DISTANCE * calculationParams.iteration + VizualConstants.VERTICAL_SHIFT;
    const outputTextLowerBoundary = '<' + formatNumberToGivenPrecision(calculationParams.prevCharLowerBoundary);
    const outputTextUpperBoundary = formatNumberToGivenPrecision(calculationParams.prevCharUpperBoundary) + ')';

    drawing.text(outputTextLowerBoundary).attr({ x: xCoordinate1, y: yCoordinate1 });
    xCoordinate1 = VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    drawing.text(outputTextUpperBoundary).attr({ x: xCoordinate1, y: yCoordinate1 });
}

/**
 * @param {Object}  drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 * @param {import("../compression/vizualizeEncodingIteration").CalculationParameters} calculationParams
 */
export function drawCalculationText(drawing, calculationParams) {
    let outputText = HtmlEncode(calculationParams.char) + ":";
    let xCoordinate1 = VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT + 220;
    let yCoordinate1 = VizualConstants.VERTICAL_DISTANCE * calculationParams.iteration + VizualConstants.VERTICAL_SHIFT;
    let intervalLength = calculationParams.prevCharIntervalLength;
    const iterationSubscript = calculationParams.iteration + 1;

    drawing.text(outputText).font({ fill: "red", size: 20 }).attr({ x: xCoordinate1, y: yCoordinate1 });

    xCoordinate1 = VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT + 250;
    let foreignObject = drawing.foreignObject(800, VizualConstants.VERTICAL_DISTANCE).attr({ x: xCoordinate1, y: yCoordinate1 });
    const lowerBoundaryEquation = '<p>`D_' + iterationSubscript.toString() + ' = D_' + (iterationSubscript - 1).toString()
        + ' + frac{m_' + HtmlEncode(calculationParams.char) + '}{n} * L_' + (iterationSubscript - 1).toString() + ' = ' + formatNumberToGivenPrecision(calculationParams.prevCharLowerBoundary)
        + ' + frac{' + calculationParams.charFrequencyWithSmallerOrd[calculationParams.char] + '}{' + calculationParams.stringLength
        + '} * ' + formatNumberToGivenPrecision(intervalLength) + ' = ' + formatNumberToGivenPrecision(calculationParams.currentCharLowerBoundary) + '`</p>';
    const upperBoundaryEquation = '<p>`H_' + iterationSubscript.toString() + ' = D_' + iterationSubscript.toString()
        + ' + frac{n_' + HtmlEncode(calculationParams.char) + '}{n} * L_' + (iterationSubscript - 1).toString() + ' = ' + formatNumberToGivenPrecision(calculationParams.currentCharLowerBoundary)
        + ' + frac{' + calculationParams.charFrequency[calculationParams.char] + '}{' + calculationParams.stringLength + '} * '
        + formatNumberToGivenPrecision(intervalLength) + ' = ' + formatNumberToGivenPrecision(calculationParams.currentCharUpperBoundary) + '`</p>';
    const lengthEquation = '<p>`L_' + iterationSubscript.toString() + ' = H_' + iterationSubscript.toString() + ' - '
        + 'D_' + iterationSubscript.toString() + ' = ' + formatNumberToGivenPrecision(calculationParams.currentCharUpperBoundary) + ' - '
        + formatNumberToGivenPrecision(calculationParams.currentCharLowerBoundary) + ' = ' + formatNumberToGivenPrecision(calculationParams.currentCharIntervalLength) + '`</p>';

    foreignObject.add(lowerBoundaryEquation);
    foreignObject.add(upperBoundaryEquation);
    foreignObject.add(lengthEquation);

    outputText = 'Znak "' + HtmlEncode(calculationParams.char) + '" leží v intervalu <' + formatNumberToGivenPrecision(calculationParams.currentCharLowerBoundary) + '; ' + formatNumberToGivenPrecision(calculationParams.currentCharUpperBoundary) + ')';
    yCoordinate1 = yCoordinate1 + 120;
    drawing.text(outputText).font({ fill: "red", size: 15 }).attr({ x: xCoordinate1, y: yCoordinate1 });
}

/**
 * @param {Object}  drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 * @param {import("../compression/vizualizeEncodingIteration").CalculationParameters} calculationParams
 */
export function drawSubinterval(drawing, calculationParams) {
    const x1 = getNewLowerBoundary(0, calculationParams.char, calculationParams.charFrequencyWithSmallerOrd, 1);
    const x2 = getNewUpperBoundary(x1, calculationParams.char, calculationParams.charFrequency, 1);
    const charLenght = x2 - x1;
    const iterationSubscript = calculationParams.iteration + 1;
    let xCoordinate1, xCoordinate2, yCoordinate1, yCoordinate2;
    let outputText;

    xCoordinate1 = x1 * VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT - 20;
    yCoordinate1 = VizualConstants.VERTICAL_DISTANCE * calculationParams.iteration + VizualConstants.VERTICAL_SHIFT - 25;
    outputText = 'D = ' + formatNumberToGivenPrecision(calculationParams.currentCharLowerBoundary);
    drawing.text(outputText).attr({ x: xCoordinate1, y: yCoordinate1 }).font({ size: 12, fill: "red" });

    xCoordinate1 = xCoordinate1 + 8;
    yCoordinate1 = yCoordinate1 + 6;
    outputText = iterationSubscript.toString();
    drawing.text(outputText).attr({ x: xCoordinate1, y: yCoordinate1 }).font({ size: 9, fill: "red" });

    xCoordinate1 = (x1 + charLenght) * VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    yCoordinate1 = VizualConstants.VERTICAL_DISTANCE * calculationParams.iteration + VizualConstants.VERTICAL_SHIFT - 25;
    outputText = 'H = ' + formatNumberToGivenPrecision(calculationParams.currentCharUpperBoundary);
    drawing.text(outputText).attr({ x: xCoordinate1, y: yCoordinate1 }).font({ size: 12, fill: "red" });

    xCoordinate1 = (x1 + charLenght) * VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT + 8;
    yCoordinate1 = VizualConstants.VERTICAL_DISTANCE * calculationParams.iteration + VizualConstants.VERTICAL_SHIFT - 25 + 6;
    outputText = iterationSubscript.toString();
    drawing.text(outputText).attr({ x: xCoordinate1, y: yCoordinate1 }).font({ size: 9, fill: "red" });

    xCoordinate1 = x1 * VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    yCoordinate1 = VizualConstants.VERTICAL_DISTANCE * calculationParams.iteration + VizualConstants.VERTICAL_SHIFT;
    xCoordinate2 = (x1 + charLenght) * VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    yCoordinate2 = yCoordinate1;
    drawing.line(xCoordinate1, yCoordinate1, xCoordinate2, yCoordinate2).stroke({ width: 2, color: "red" });
}

export function drawNameOfSubinterval(draw, calculationParams) {
    const x1 = getNewLowerBoundary(0, calculationParams.char, calculationParams.charFrequencyWithSmallerOrd, 1);
    const x2 = getNewUpperBoundary(x1, calculationParams.char, calculationParams.charFrequency, 1);
    const charLenght = x2 - x1;
    const middlePoint = x1 + charLenght / 2;
    const xCoordinate1 = middlePoint * VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    const yCoordinate1 = (VizualConstants.VERTICAL_DISTANCE * calculationParams.iteration) + 15 + VizualConstants.VERTICAL_SHIFT;
    const outputText = HtmlEncode(calculationParams.char);

    draw.text(outputText).font({ fill: "red" }).attr({ x: xCoordinate1, y: yCoordinate1 });
}

export function drawArrowsToNewIteration(draw, calculationParams) {
    if (calculationParams.iteration == calculationParams.stringLength) {
        return;
    }

    const x1 = getNewLowerBoundary(0, calculationParams.char, calculationParams.charFrequencyWithSmallerOrd, 1);
    const x2 = getNewUpperBoundary(x1, calculationParams.char, calculationParams.charFrequency, 1);
    const charLenght = x2 - x1;
    const arrow = getArrowMarker(draw);
    const dot = getDotMarker(draw);

    let xCoordinate1, yCoordinate1, xCoordinate2, yCoordinate2;

    xCoordinate1 = x1 * VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    yCoordinate1 = VizualConstants.VERTICAL_DISTANCE * calculationParams.iteration + VizualConstants.VERTICAL_SHIFT;
    xCoordinate2 = 0 + VizualConstants.HORIZONTAL_SHIFT;
    yCoordinate2 = VizualConstants.VERTICAL_DISTANCE * (calculationParams.iteration + 1) + VizualConstants.VERTICAL_SHIFT;
    let line1 = draw.line(xCoordinate1, yCoordinate1, xCoordinate2, yCoordinate2).stroke({ width: 1, color: "grey", dasharray: 4 }).marker('end', arrow);
    line1.marker('start', dot);

    xCoordinate1 = (x1 + charLenght) * VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    yCoordinate1 = VizualConstants.VERTICAL_DISTANCE * calculationParams.iteration + VizualConstants.VERTICAL_SHIFT;
    xCoordinate2 = VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    yCoordinate2 = VizualConstants.VERTICAL_DISTANCE * (calculationParams.iteration + 1) + VizualConstants.VERTICAL_SHIFT;
    let line2 = draw.line(xCoordinate1, yCoordinate1, xCoordinate2, yCoordinate2).stroke({ width: 1, color: "grey", dasharray: 4 }).marker('end', arrow);
    line2.marker('start', dot);

}

/**
 * 
 * @param {Object} drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 * @param {string} finalNumber
 * @param {import("../compression/vizualizeEncodingIteration").CalculationParameters} calculationsParams
 */
export function drawFinalNumberCreation(drawing, finalNumber, calculationsParams) {

    const iteration = calculationsParams.iteration;
    const numOfDigits = finalNumber.toString().length;
    const lowerBoundary = calculationsParams.prevCharLowerBoundary;
    const upperBoundary = calculationsParams.prevCharUpperBoundary;
    let index;

    for (index = 0; index < formatNumberToGivenPrecision(lowerBoundary).length; index++) {
        if (index < numOfDigits) {
            drawing.text(formatNumberToGivenPrecision(lowerBoundary)[index])
                .font({ size: 25, fill: "green" })
                .attr({
                    x: 0 + VizualConstants.HORIZONTAL_SHIFT + 100 + (index * 20),
                    y: VizualConstants.VERTICAL_DISTANCE * iteration + VizualConstants.VERTICAL_SHIFT + 100
                });
        } else {
            drawing.text(formatNumberToGivenPrecision(lowerBoundary)[index])
                .font({ size: 25 })
                .attr({
                    x: 0 + VizualConstants.HORIZONTAL_SHIFT + 100 + (index * 20),
                    y: VizualConstants.VERTICAL_DISTANCE * iteration + VizualConstants.VERTICAL_SHIFT + 100
                });
        }

    }

    for (index = 0; index < formatNumberToGivenPrecision(upperBoundary).length; index++) {
        if (index < (numOfDigits - 1)) {
            drawing.text(formatNumberToGivenPrecision(upperBoundary)[index])
                .font({ size: 25, fill: "green" })
                .attr({
                    x: 0 + VizualConstants.HORIZONTAL_SHIFT + 100 + (index * 20),
                    y: VizualConstants.VERTICAL_DISTANCE * iteration + VizualConstants.VERTICAL_SHIFT + 150
                });
        } else if (index == (numOfDigits-1)) {
            drawing.text(formatNumberToGivenPrecision(upperBoundary)[index])
                .font({ size: 25, fill: "#FFC300" })
                .attr({
                    x: 0 + VizualConstants.HORIZONTAL_SHIFT + 100 + (index * 20),
                    y: VizualConstants.VERTICAL_DISTANCE * iteration + VizualConstants.VERTICAL_SHIFT + 150
                });
        } else {
            drawing.text(formatNumberToGivenPrecision(upperBoundary)[index])
                .font({ size: 25 })
                .attr({
                    x: 0 + VizualConstants.HORIZONTAL_SHIFT + 100 + (index * 20),
                    y: VizualConstants.VERTICAL_DISTANCE * iteration + VizualConstants.VERTICAL_SHIFT + 150
                });
        }

    }

    for (index = 0; index < formatNumberToGivenPrecision(upperBoundary).length; index++) {
        if (index < numOfDigits) {
            drawing.text(finalNumber.toString()[index])
                .font({ size: 25, fill: "red" })
                .attr({
                    x: 0 + VizualConstants.HORIZONTAL_SHIFT + 100 + (index * 20),
                    y: VizualConstants.VERTICAL_DISTANCE * iteration + VizualConstants.VERTICAL_SHIFT + 220
                });
        } else {
            drawing.text("0")
                .font({ size: 25, fill: "red" })
                .attr({
                    x: 0 + VizualConstants.HORIZONTAL_SHIFT + 100 + (index * 20),
                    y: VizualConstants.VERTICAL_DISTANCE * iteration + VizualConstants.VERTICAL_SHIFT + 220
                });
        }
    }

}

/**
 * @param {Object} drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 * @param {import("../typedefs.js").CalculationParameters} calculationParams
 */
export function drawCharIntervalAndPosOfEncodedNum(drawing, calculationParams) {
    // ziskam si vizualni hranice intervalu pro aktualne deokodovany znak
    // interval nemusi lezet mezi 0 a 1, muze lezet mezi daleko blizsimi
    // cisly, ale z vizualniho hlediska budou vsechny intervaly
    // normovany na stejnou delku, tou je vzdalenost mezi 0 a 1.
    const charFrequencyWithSmallerOrd = calculationParams.charFrequencyWithSmallerOrd;
    const charsFrequency = calculationParams.charFrequency;
    const char = calculationParams.char;
    const vizualCharIntervalStart = getNewLowerBoundary(0, char, charFrequencyWithSmallerOrd, 1);
    const vizualCharIntervalEnd = getNewUpperBoundary(vizualCharIntervalStart, char, charsFrequency, 1);
    const vizualCharIntervalLenght = vizualCharIntervalEnd - vizualCharIntervalStart;

    // vytvorim cervenou caru ktera predstavuje interval prave vybraneho znaku 
    // pocatecni koordinat x1 (zacatek intervalu prave vybraneho znaku) krat delka tluste cary
    // plus horizontalni posun (touto aritmetikou dostanu pocatek cary intervalu vybraneho znaku)
    const xCoordinate = vizualCharIntervalStart * VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    const yCoordinate = VizualConstants.HEIGHT;
    const yEndCoordinate = yCoordinate;
    // pocatek intervalu x1 plus delka intervalu patriciho vybranmu znaku se rovna souradnici
    // v x-ose koncoveho bodu intervalu prave vybraneho znaku
    const xEndCoordinate = (vizualCharIntervalStart + vizualCharIntervalLenght) * VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    drawing.line(xCoordinate, yCoordinate, xEndCoordinate, yEndCoordinate).stroke({ width: 2, color: "red" });

    /** @type {import("../typedefs.js").IntervalBoundaries} */
    const currentInterval = {
        lowerBoundary: calculationParams.currentCharLowerBoundary,
        upperBoundary: calculationParams.currentCharUpperBoundary
    }

    /** @type {import("../typedefs.js").IntervalBoundaries} */
    const vizualCharInterval = {
        lowerBoundary: vizualCharIntervalStart,
        upperBoundary: vizualCharIntervalEnd
    }

    /** @type {import("../typedefs.js").ParamsCharIntervalLegend} */
    let params = {
        iteration: calculationParams.iteration,
        vizualCharInterval: vizualCharInterval,
        currentInterval: currentInterval
    }

    drawLegendForVizualCharInterval(drawing, params);
    drawHeaderForCharInterval(drawing, params, calculationParams.char);
    drawEncodedNumberLocation(drawing, params, calculationParams.encodedNumber);
}

/**
 * @param {Object} drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 * @param {import("../typedefs.js").ParamsCharIntervalLegend} params
 */
function drawHeaderForCharInterval(drawing, params, char) {
    const vizualCharIntervalStart = params.vizualCharInterval.lowerBoundary;
    const vizualCharIntervalLenght = params.vizualCharInterval.upperBoundary - params.vizualCharInterval.lowerBoundary;

    // vyobrazeni pismena ktere je reprezentovano cervenym intervalem, ktery byl vykreslen v predchozich prikazech
    // pismeno je v puli intervalu 45 pixelu nahoru od nej
    const xCoordinate = (vizualCharIntervalStart + vizualCharIntervalLenght / 2) * VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    const yCoordinate = VizualConstants.HEIGHT - 45;
    drawing.text(HtmlEncode(char)).attr({ x: xCoordinate, y: yCoordinate }).font({ size: 15, fill: "red" });
}

/**
 * @param {Object} drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 * @param {import("../typedefs.js").ParamsCharIntervalLegend} params
 */
function drawLegendForVizualCharInterval(drawing, params) {
    const vizualCharIntervalStart = params.vizualCharInterval.lowerBoundary;
    const vizualCharIntervalLenght = params.vizualCharInterval.upperBoundary - params.vizualCharInterval.lowerBoundary;
    const currentCharLowerBoundary = params.currentInterval.lowerBoundary;
    const currentCharUpperBoundary = params.currentInterval.upperBoundary;
    const iteration = params.iteration;

    // vytvorim legendu k pocatecni hranici intervalu prave vybraneho znaku
    let xCoordinate = vizualCharIntervalStart * VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT - 20;
    let yCoordinate = VizualConstants.HEIGHT - 25;
    drawing.text('D = ' + formatNumberToGivenPrecision(currentCharLowerBoundary)).attr({ x: xCoordinate, y: yCoordinate }).font({ size: 12, fill: "red" });

    // vytvorim index k predchozi legende
    xCoordinate = xCoordinate + 8;
    yCoordinate = yCoordinate + 6;
    drawing.text(iteration.toString()).attr({ x: xCoordinate, y: yCoordinate }).font({ size: 9, fill: "red" });

    // vytvorim legendu ke koncove hranici intervalu prave vybraneho znaku
    // pocatecni koordinat x1 (zacatek intervalu prave vybraneho znaku) + delka intervalu patriciho k vybranemu znaku vynasobena umele stanovenou delkou
    // tluste cary plus horiontalni posun
    xCoordinate = (vizualCharIntervalStart + vizualCharIntervalLenght) * VizualConstants.WIDTH + VizualConstants.HORIZONTAL_SHIFT;
    yCoordinate = VizualConstants.HEIGHT - 25;
    drawing.text('H = ' + formatNumberToGivenPrecision(currentCharUpperBoundary)).attr({ x: xCoordinate, y: yCoordinate }).font({ size: 12, fill: "red" });

    // vytvorim index k predchozi legende
    xCoordinate = xCoordinate + 8;
    yCoordinate = yCoordinate + 6;
    drawing.text(iteration.toString()).attr({ x: xCoordinate, y: yCoordinate }).font({ size: 9, fill: "red" });
}

/**
 * @param {Object} drawing - SVG.Svg class from SVG.js libary, descendant of SVG.Container
 * @param {import("../typedefs.js").ParamsCharIntervalLegend} params
 * @param {number} encodedNumber
 */
function drawEncodedNumberLocation(drawing, params, encodedNumber) {
    const currentCharIntervalLength = params.currentInterval.upperBoundary - params.currentInterval.lowerBoundary;
    const currentCharLowerBoundary = params.currentInterval.lowerBoundary;
    const vizualCharIntervalStart = params.vizualCharInterval.lowerBoundary;
    const vizualCharIntervalLenght = params.vizualCharInterval.upperBoundary - params.vizualCharInterval.lowerBoundary;
    let boundaryLength = currentCharIntervalLength;
    let distanceFromLowBoundary = encodedNumber - currentCharLowerBoundary;
    // pointLocation predstavuje souradnici v x-ose bodu, ktery znazornuje
    // pozici cisla do nejz je zakodovana originalni zprava
    let pointLocation = distanceFromLowBoundary / boundaryLength;
    // vyobrazim modrou vertikalni carou o delce 20 pixelu pozici cisla reprezentujiciho originalni zpravu
    const xCoordinate = ((vizualCharIntervalStart + (vizualCharIntervalLenght * pointLocation)) * VizualConstants.WIDTH) + VizualConstants.HORIZONTAL_SHIFT;
    let yCoordinate = VizualConstants.HEIGHT - 10;
    let xCoordinate2 = xCoordinate;
    let yCoordinate2 = VizualConstants.HEIGHT + 10;
    drawing.line(xCoordinate, yCoordinate, xCoordinate2, yCoordinate2).stroke({ width: 1, color: "#0E49FF" });
    yCoordinate = VizualConstants.HEIGHT + 5;
    // k care pripisu hodnotu tohoto cisla
    drawing.text(encodedNumber.toString()).attr({ x: xCoordinate, y: yCoordinate }).font({ size: 12, fill: "#0E49FF" });
}

export function drawDecodingInformation(calculationParams) {
    const containerDiv = createFlexContainer();
    appendChild(containerDiv, 'drawing-decoding');
    drawEquationsOfDecodingIteration(calculationParams, containerDiv);
    drawDescriptionOfDecodingIteration(calculationParams, containerDiv)
}

function drawEquationsOfDecodingIteration(calculationParams, container) {

    // ziskam si hranice intervalu prave dekodovaneho znaku
    let lowerBoundaryEquation = getLowerBoundaryEquation(calculationParams);
    let upperBoundaryEquation = getUpperBoundaryEquation(calculationParams);
    let lengthEquation = getLengthEquation(calculationParams);
    const char = calculationParams.char;
    const charHeaderColumn = createHtmlElement("div", ["character-box"]);
    container.appendChild(charHeaderColumn);
    const charHeader = char + ":";
    let p = createParagraphElement(charHeader, ["text-danger", "lead", "char"]);
    charHeaderColumn.appendChild(p);
    const equationColumn = createHtmlElement("div", ["equation-box"]);
    container.appendChild(equationColumn);
    p = createParagraphElement(lowerBoundaryEquation);
    equationColumn.appendChild(p);
    p = createParagraphElement(upperBoundaryEquation);
    equationColumn.appendChild(p);
    p = createParagraphElement(lengthEquation);
    equationColumn.appendChild(p);
}

function drawDescriptionOfDecodingIteration(calculationParams, container) {
    const descriptionColumn = createHtmlElement("div", ["description-box", "mt-4"]);
    const currentCharLowerBoundary = calculationParams.currentCharLowerBoundary;
    const currentCharUpperBoundary = calculationParams.currentCharUpperBoundary;
    const prevCharLowerBoundary = calculationParams.prevCharLowerBoundary;
    const prevCharUpperBoundary = calculationParams.prevCharUpperBoundary;
    const decodedMessage = calculationParams.decodedMsg;
    const encodedNumber = calculationParams.encodedNumber;
    const iteration = calculationParams.iteration;
    const char = calculationParams.char;
    
    container.appendChild(descriptionColumn);

    let descriptionTexts = [];

    const newLowerBoundaryFormatted = formatNumberToGivenPrecision(currentCharLowerBoundary);
    const newUpperBoundaryFormatted = formatNumberToGivenPrecision(currentCharUpperBoundary);
    const proofOfEncodedNumPosition = 'O zakódovaném číslu <span class="encoded-number">' + encodedNumber + '</span> platí: `' + encodedNumber + ' ge ' + newLowerBoundaryFormatted
        + ' ^^ ' + encodedNumber + ' lt ' + newUpperBoundaryFormatted + '`, z toho vyplývá, že leží v intervalu <<span class="interval-numbers">'
        + newLowerBoundaryFormatted + '; ' + newUpperBoundaryFormatted + '</span>).';
    descriptionTexts.push(proofOfEncodedNumPosition);

    const oldLowerBoundaryFormatted = formatNumberToGivenPrecision(prevCharLowerBoundary);
    const oldUpperBoundaryFormatted = formatNumberToGivenPrecision(prevCharUpperBoundary);
    const whichCharBelongsToInterval = 'V ' + iteration + '. iteraci pracujeme s intervalem <strong><' + oldLowerBoundaryFormatted + '; ' + oldUpperBoundaryFormatted + ')</strong>. '
        + 'Na tomto intervalu je znaku "<span class="text-danger">' + char + '</span>" přiřazen interval <'
        + newLowerBoundaryFormatted + '; ' + newUpperBoundaryFormatted + '). Tudíž ' + iteration + '. dekódovaným znakem je znak: <strong>' + char + '</strong>.';
    descriptionTexts.push(whichCharBelongsToInterval);

    const outputOfTheIteration = 'Výstupem této iterace je řetězec: <span class="text-success">' + decodedMessage + '</class>';
    descriptionTexts.push(outputOfTheIteration);

    descriptionTexts.forEach(description => {
        let p = createParagraphElement(description);
        descriptionColumn.appendChild(p);
    });


    container.appendChild(descriptionColumn);

}
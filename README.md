# README #

### What is this repository for? ###

This is source code for my bachelor thesis. It's purpose is to vizualize arithmetic compression algorithm.

### How do I get set up? ###
 
You need web server like apache in order to run this application. Also you will have to have NodeJs and NPM installed in order to install necessary dependencies. 
Once you've installed NPM and NodeJS you can run the installation process from command line by executing `npm install`. In order to transpile scss and js files, execute `npm run watch`. Make sure you are in the root folder of the project before you execute those commands.

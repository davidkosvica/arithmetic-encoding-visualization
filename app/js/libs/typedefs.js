/**
 * Crate containing data for drawings functions and 
 * calculation functions that are necessary 
 * to perform iteration of arithmetic compression.
 * @typedef {Object} CalculationParameters
 * @property {number} iteration - Current interation of the compression algorithm
 * @property {number} encodedNumber - Output number from arithmetic compression, represents original message
 * @property {number} currentCharLowerBoundary - Beginning of the current interval
 * @property {number} currentCharUpperBoundary - Ending of the current interval
 * @property {number} prevCharLowerBoundary - Beginning of the previous interval
 * @property {number} prevCharUpperBoundary - Ending of the previous interval
 * @property {number} prevCharIntervalLength - Length of the previous interval
 * @property {number} currentCharIntervalLength - Length of the current interval
 * @property {Object.<string, number>} charFrequency - Object containing alphabet of the message to be encoded and frequency of each character
 * @property {Object.<string, number>} charFrequencyWithSmallerOrd - Object containing alphabet of the message and frequency of characters with smaller ASCII value
 * @property {number} stringLength - Number of characters in the message to be encoded
 * @property {string} char - Character which is being encoded in current iteration
 * @property {string} decodedMsg - String used during decoding, it contains decoded characters 
 * from the encoded compressed message, it grows in lenght during decompression through each iteration
 */
 
 /**
 * Interval crate, serves to transport intervals.
 * @typedef {Object} IntervalBoundaries
 * @property {number} lowerBoundary - Beginning of the interval.
 * @property {number} upperBoundary - Ending of the interval.
 * 
 */
 
 /** 
 * Crate containing data that are exchanged between
 * each iteration of the compression algorithm.
 * @typedef {Object} CompressionDataSet
 * @property {string} originalMessage - String to be compressed using arithmetic compression algorithm.
 * @property {string} encodedMessage - Encoded message. Contains frequency of each letter and output number from compression algorithm.
 * @property {number} encodedNumber - Output number resulted from arithmetic algorithm representing original data.
 * @property {string} decodedMessage - Result of decoding algorithm
 * @property {Object<string, number>} charFrequency - Frequency of individual letters from the string to encode.
 * @property {Object<string, number>} charFrequencyWithSmallerOrd - Frequency of letters with smaller ASCII value for individual letters from the string to encode.
 * @property {number} lowerBoundary - Indicates lower boundary for interval for any given iteration of compression
 * @property {number} upperBoundary - Indicates upper boundary for interval for any given iteration of compression
 * @property {number} iteration - Current ordinal number for iteration (1st iteration, 2nd interation, 3th ...)
 * @property {number} EPSILON - Minimum difference between two floating numbers
 */

 /**
 * Crate to contain data needed to draw legend descriptions of char intervals on main interval
 * @typedef {Object} ParamsCharIntervalLegend
 * @property {number} iteration
 * @property {IntervalBoundaries} vizualCharInterval
 * @property {IntervalBoundaries} currentInterval
 */


export default {}

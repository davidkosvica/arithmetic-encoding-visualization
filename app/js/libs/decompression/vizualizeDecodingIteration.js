'use strict';

import {
    createHTMLElemIterationHeader,
    appendChild,
    getNewLowerBoundary,
    getNewUpperBoundary,
    showBlockNode,
    isEncodedNumberInInterval,
    hideElemsWithId,
    createFinalDecodeDescription
} from "../helpers.js";
import { VizualConstants } from '../vizuals/vizualConstants.js';
import {
    getSVGElemAndAppendToElemWithId,
    drawMainLineForDecoding,
    drawBoundaryTextForDecoding,
    drawCharIntervalAndPosOfEncodedNum,
    drawDecodingInformation
} from "../vizuals/drawingFunctions.js";
/**
 * 
 * @param {import("../typedefs.js").compressionDataSet} dataSet 
 */
export function vizualizeDecodingIteration(dataSet) {
    const encodedNumber = dataSet.encodedNumber;
    const charsFreq = dataSet.charFrequency;
    const charFreqWithSmallerOrd = dataSet.charFrequencyWithSmallerOrd;
    // ulozim si hranice intervalu z predchozi iterace
    // jedna se vlastne o interval predchoziho znaku
    // v tomto intervalu bude lezet i interval nasledujici
    let prevCharLowerBoundary = dataSet.lowerBoundary;
    let prevCharUpperBoundary = dataSet.upperBoundary;
    let prevCharIntervalLength = prevCharUpperBoundary - prevCharLowerBoundary;
    let decodedMessage = dataSet.decodedMessage;

    if (dataSet.originalMessage.length < dataSet.iteration) {
        createFinalDecodeDescription(decodedMessage);
        hideElemsWithId(['iterate-decode-btn']);
    } else {
        showBlockNode('drawing-decoding');
        const iteration = dataSet.iteration;
        const iterHdr = createHTMLElemIterationHeader(iteration.toString());
        appendChild(iterHdr, 'drawing-decoding');
        const drawing = getSVGElemAndAppendToElemWithId('#drawing-decoding', VizualConstants.MAIN_DRAWING_WIDTH, VizualConstants.MAIN_DRAWING_HEIGHT);
        // prochazej abecedu originalni zpravy
        for (let char in charsFreq) {
            // ziskam hranice intervalu a jeho delku pro aktualne vybrany znak z abecedy zpravy
            let currentCharLowerBoundary = getNewLowerBoundary(prevCharLowerBoundary, char, charFreqWithSmallerOrd, prevCharIntervalLength);
            let currentCharUpperBoundary = getNewUpperBoundary(currentCharLowerBoundary, char, charsFreq, prevCharIntervalLength);
            let currentCharIntervalLength = currentCharUpperBoundary - currentCharLowerBoundary;

            /**
             * @type {import("../typedefs.js").CalculationParameters}
             */
            let calculationParams = {
                iteration: iteration,
                encodedNumber: encodedNumber,
                currentCharLowerBoundary: currentCharLowerBoundary,
                currentCharUpperBoundary: currentCharUpperBoundary,
                currentCharIntervalLength: currentCharIntervalLength,
                prevCharLowerBoundary: prevCharLowerBoundary,
                prevCharUpperBoundary: prevCharUpperBoundary,
                prevCharIntervalLength: prevCharIntervalLength,
                charFrequency: charsFreq,
                charFrequencyWithSmallerOrd: charFreqWithSmallerOrd,
                stringLength: dataSet.originalMessage.length,
                char: char
            };


            /** @type {import("../typedefs.js").IntervalBoundaries} */
            const currentInterval = {
                lowerBoundary: currentCharLowerBoundary,
                upperBoundary: currentCharUpperBoundary,
            }

            if (isEncodedNumberInInterval(encodedNumber, currentInterval)) {
                // jedna se o zpravny znak, vloz ho do jiz dekodovanych znaku 
                decodedMessage += char;
                calculationParams.decodedMsg = decodedMessage;

                drawMainLineForDecoding(drawing);
                drawBoundaryTextForDecoding(drawing, calculationParams);
                drawCharIntervalAndPosOfEncodedNum(drawing, calculationParams);
                drawDecodingInformation(calculationParams);

                dataSet.lowerBoundary = currentCharLowerBoundary;
                dataSet.upperBoundary = currentCharUpperBoundary;
                dataSet.decodedMessage = decodedMessage;
                dataSet.iteration++;

                break;
            }
        }
    }

    MathJax.typeset();
}